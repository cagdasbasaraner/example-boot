package com.product.repository;

import com.product.model.Product;
import org.junit.Before;
import org.junit.Test;

import java.util.Optional;
import java.util.UUID;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

public class ProductRepositoryTest {

    private ProductRepository productRepository;

    @Before
    public void setUp() {
        productRepository = new ProductRepositoryImpl();
    }

    @Test
    public void testAdd() {
        Product product = generateProduct();
        Product returnedProduct =  productRepository.add(product);

        assertThat(returnedProduct.getName(), is(product.getName()));
        assertThat(returnedProduct.getPrice(), is(product.getPrice()));
        assertThat(returnedProduct.getStock(), is(product.getStock()));

        Product returnedProduct2 = productRepository.add(product);// id is discarded, no exception
        assertNotEquals(returnedProduct2.getId(), returnedProduct.getId());
    }

    @Test
    public void testRemove() {
        Product product = generateProduct();
        Product returnedProduct =  productRepository.add(product);
        productRepository.remove(returnedProduct.getId());
        Optional<Product> productAfterRemove = productRepository.get(returnedProduct.getId());

        assertFalse(productAfterRemove.isPresent());
    }

    @Test
    public void testUpdate() {

    }

    @Test
    public void testGet() {

    }

    @Test
    public void testList() {

    }

    private Product generateProduct() {
        Product product = new Product();
        product.setStock(1);
        product.setPrice(10f);
        product.setName(UUID.randomUUID().toString());
        return product;
    }
}
