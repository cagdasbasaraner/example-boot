package com.product;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.*;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.is;

public class CollectionsLambdaTest {

    private Map<String, Integer> countsMap = new HashMap<>();

    @Before
    public void  setUp() {
        countsMap = new HashMap<>();
    }

    @Test
    public void testAddValuesToCountMap() {

        countsMap.put("1", 5);
        countsMap.put("2", 3);
        countsMap.put("3", 2); //smallest
        countsMap.put("4", 8); // biggest

        // TODO
    }

    @Test
    public void testMapSortByKey() {

        countsMap.put("3", 2); //smallest
        countsMap.put("2", 3);
        countsMap.put("1", 5);
        countsMap.put("4", 8); // biggest

        // sort increasing
        List<Integer> convertedList = countsMap.entrySet().stream().sorted(Map.Entry.comparingByKey()).map(x -> x.getValue()).collect(Collectors.toList());
        Assert.assertThat(convertedList.get(0), is(5));

        // sort decreasing
        convertedList = countsMap.entrySet().stream().sorted(Collections.reverseOrder(Map.Entry.comparingByKey())).map(x -> x.getValue()).collect(Collectors.toList());
        Assert.assertThat(convertedList.get(0), is(8));
    }

    @Test
    public void testMapSortByKeyWithTreeMap() {
        countsMap.put("3", 2); //smallest
        countsMap.put("2", 3);
        countsMap.put("1", 5);
        countsMap.put("4", 8); // biggest

        TreeMap treeMap = new TreeMap<>(countsMap);
        Assert.assertThat(treeMap.firstKey(), is("1"));
        Assert.assertThat(treeMap.lastKey(), is("4"));
    }

    @Test
    public void testMapSortByValue() {

        countsMap.put("1", 5);
        countsMap.put("2", 3);
        countsMap.put("3", 2); //smallest
        countsMap.put("4", 8); // biggest

        // sort increasing
        List<String> convertedList = countsMap.entrySet().stream().sorted(Map.Entry.comparingByValue()).map(x -> x.getKey()).collect(Collectors.toList());
        Assert.assertThat(convertedList.get(0), is("3"));

        // sort decreasing
        convertedList = countsMap.entrySet().stream().sorted(Collections.reverseOrder(Map.Entry.comparingByValue())).map(x -> x.getKey()).collect(Collectors.toList());
        Assert.assertThat(convertedList.get(0), is("4"));
    }

    @Test
    public void testMapSortByKeyAndValue() {

        countsMap.put("1", 5);
        countsMap.put("2", 3);
        countsMap.put("4", 8);
        countsMap.put("3", 8); // smallest key, biggest value
        countsMap.put("5", 8);

        Comparator<Map.Entry<String, Integer>> comparator = Comparator
                .comparing(Map.Entry<String, Integer>::getValue, Comparator.reverseOrder())
                .thenComparing(Map.Entry::getKey);

        // sort decreasing
        List<String> convertedList = countsMap.entrySet().stream().sorted(comparator).map(x -> x.getKey()).collect(Collectors.toList());
        Assert.assertThat(convertedList.get(0), is("3"));
    }
}
