package com.product.exception;

public class ProductExistsException extends Throwable {
    public ProductExistsException(String message) {
        super(message);
    }
}