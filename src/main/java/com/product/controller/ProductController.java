package com.product.controller;

import com.product.exception.ProductNotFoundException;
import com.product.mapper.ProductDtoMapper;
import com.product.model.Product;
import com.product.model.payload.ProductPayload;
import com.product.model.payload.ProductStockPayload;
import com.product.model.response.ProductListResponse;
import com.product.model.response.ProductResponse;
import com.product.service.ProductServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.websocket.server.PathParam;

@RestController
@RequestMapping("/products")
public class ProductController {

    @Autowired
    private ProductServiceImpl productService;

    @Autowired
    private ProductDtoMapper productDtoMapper;

    @RequestMapping(value = "/{productId}", method = RequestMethod.GET)
    public ResponseEntity getProduct(@PathParam(value = "productId") String productId) throws ProductNotFoundException {
        Product product = productService.getProduct(productId);
        ProductResponse productResponse = productDtoMapper.constructResponseFromProduct(product);
        return ResponseEntity.ok(productResponse);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity addProduct(@Valid @RequestBody ProductPayload productPayload) throws ProductNotFoundException {
        Product product = productDtoMapper.constructProductFromPayload(productPayload);
        productService.addProduct(product);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/{productId}", method = RequestMethod.PUT)
    public ResponseEntity updateProduct(@Valid @RequestBody ProductPayload productPayload,
                                        @PathParam(value = "productId") String productId) throws ProductNotFoundException {
        Product product = productDtoMapper.constructProductFromPayload(productId, productPayload);
        productService.updateProduct(product);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/{productId}", method = RequestMethod.PATCH)
    public ResponseEntity updateProductStock(@Valid @RequestBody ProductStockPayload productStockPayload,
                                             @PathParam(value = "productId") String productId) throws ProductNotFoundException {
        productService.updateProductStock(productId, productStockPayload.getStock());
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/{productId}", method = RequestMethod.DELETE)
    public ResponseEntity deleteProduct(@PathParam(value = "productId") String productId) throws ProductNotFoundException {
        productService.deleteProduct(productId);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity getAllProducts() {
        productService.listAllProducts();
        ProductListResponse productListResponse = productDtoMapper.constructResponseFromProductList(productService.listAllProducts());
        return ResponseEntity.ok(productListResponse);
    }
}
