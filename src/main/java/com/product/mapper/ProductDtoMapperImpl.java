package com.product.mapper;

import com.product.model.Product;
import com.product.model.payload.ProductPayload;
import com.product.model.response.ProductListResponse;
import com.product.model.response.ProductResponse;

import java.util.List;
import java.util.stream.Collectors;

/**
 * DTO (data transfer object) mapper for product entity.
 * Used to separate entity details with controller endpoint logic.
 */
public class ProductDtoMapperImpl implements ProductDtoMapper {

    @Override
    public Product constructProductFromPayload(String productId, ProductPayload productPayload) {
        Product product = new Product();
        product.setId(productId);
        product.setName(productPayload.getName());
        product.setStock(productPayload.getStock());
        product.setPrice(productPayload.getPrice());
        return product;
    }

    @Override
    public Product constructProductFromPayload(ProductPayload productPayload) {
        Product product = new Product();
        product.setName(productPayload.getName());
        product.setStock(productPayload.getStock());
        product.setPrice(productPayload.getPrice());
        return product;
    }

    @Override
    public ProductResponse constructResponseFromProduct(Product product) {
        ProductResponse productResponse = new ProductResponse();
        productResponse.setId(product.getId());
        productResponse.setName(product.getName());
        productResponse.setStock(product.getStock());
        productResponse.setPrice(product.getPrice());
        return productResponse;
    }

    @Override
    public ProductListResponse constructResponseFromProductList(List<Product> product) {
        ProductListResponse response = new ProductListResponse();
        response.setProducts(product.stream().map(this::constructResponseFromProduct).collect(Collectors.toList()));
        return response;
    }
}
