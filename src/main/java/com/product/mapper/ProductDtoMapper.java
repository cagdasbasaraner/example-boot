package com.product.mapper;

import com.product.model.Product;
import com.product.model.payload.ProductPayload;
import com.product.model.response.ProductListResponse;
import com.product.model.response.ProductResponse;

import java.util.List;

public interface ProductDtoMapper {

    Product constructProductFromPayload(String productId, ProductPayload productPayload);

    Product constructProductFromPayload(ProductPayload productPayload);

    ProductResponse constructResponseFromProduct(Product product);

    ProductListResponse constructResponseFromProductList(List<Product> product);
}
