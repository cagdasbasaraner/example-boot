package com.product.service;

import com.product.exception.ProductNotFoundException;
import com.product.model.Product;
import com.product.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Override
    public void deleteProduct(String productId) throws ProductNotFoundException {
        Optional<Product> product = checkProductExistence(productId);
        productRepository.remove(product.get().getId());
    }

    @Override
    public Product getProduct(String productId) throws ProductNotFoundException {
        Optional<Product> product = checkProductExistence(productId);
        return product.get();
    }

    @Override
    public void addProduct(Product product) {
        productRepository.add(product);
    }

    @Override
    public void updateProduct(Product product) throws ProductNotFoundException {
        checkProductExistence(product.getId());
        productRepository.update(product);
    }

    @Override
    public void updateProductStock(String productId, int stock) throws ProductNotFoundException {
        Optional<Product> product = checkProductExistence(productId);
        Product productToBeUpdated = product.get();
        productToBeUpdated.setStock(stock);
        productRepository.update(productToBeUpdated);
    }

    @Override
    public List<Product> listAllProducts() {
        return productRepository.list();
    }

    private Optional<Product> checkProductExistence(String productId) throws ProductNotFoundException {
        Optional<Product> product = productRepository.get(productId);
        if (!product.isPresent()) {
            throw new ProductNotFoundException("product does not exist!");
        }
        return product;
    }
}