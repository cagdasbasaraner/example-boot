package com.product.service;

import com.product.exception.ProductNotFoundException;
import com.product.model.Product;

import java.util.List;

/**
 * Interface for service layer of product operations.
 *
 * TODO : for the next version we can write another DTO mapper to break dependency between product entity and service parameters.
 */
public interface ProductService {

    Product getProduct(String productId) throws ProductNotFoundException;

    void addProduct(Product product);

    void updateProduct(Product product) throws ProductNotFoundException;

    void updateProductStock(String productId, int stock) throws ProductNotFoundException;

    void deleteProduct(String productId) throws ProductNotFoundException;

    List<Product> listAllProducts();
}
