package com.product.model.payload;

import javax.validation.constraints.Min;
import java.util.Objects;

public class ProductStockPayload {

    @Min(value = 1, message ="stock can not be zero")
    private int stock;

    public int getStock() {
        return stock;
    }

    public ProductStockPayload setStock(int stock) {
        this.stock = stock;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductStockPayload that = (ProductStockPayload) o;
        return stock == that.stock;
    }

    @Override
    public int hashCode() {
        return Objects.hash(stock);
    }
}
