package com.product.model.payload;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Objects;

public class ProductPayload {

    @NotNull (message =  "name should be defined")
    private String name;

    @Min (value = 1, message =  "stock should be defined")
    private int stock;

    @Min(value = 1, message =  "price should be defined")
    private float price;

    public String getName() {
        return name;
    }

    public ProductPayload setName(String name) {
        this.name = name;
        return this;
    }

    public int getStock() {
        return stock;
    }

    public ProductPayload setStock(int stock) {
        this.stock = stock;
        return this;
    }

    public float getPrice() {
        return price;
    }

    public ProductPayload setPrice(float price) {
        this.price = price;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductPayload product = (ProductPayload) o;
        return stock == product.stock &&
                Float.compare(product.price, price) == 0 &&
                Objects.equals(name, product.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, stock, price);
    }
}
