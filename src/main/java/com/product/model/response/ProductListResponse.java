package com.product.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ProductListResponse {

    @JsonProperty("products") // using this is not mandatory but a good practice for any variable name change possibility
    private List<ProductResponse> products = new ArrayList<>();

    public List<ProductResponse> getProducts() {
        return products;
    }

    public ProductListResponse setProducts(List<ProductResponse> products) {
        this.products = products;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductListResponse that = (ProductListResponse) o;
        return Objects.equals(products, that.products);
    }

    @Override
    public int hashCode() {
        return Objects.hash(products);
    }
}
