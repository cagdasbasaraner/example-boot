package com.product.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class ProductResponse {

    @JsonProperty("id")
    private String id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("stock")
    private int stock;

    @JsonProperty("price")
    private float price;

    public String getId() {
        return id;
    }

    public ProductResponse setId(String id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public ProductResponse setName(String name) {
        this.name = name;
        return this;
    }

    public int getStock() {
        return stock;
    }

    public ProductResponse setStock(int stock) {
        this.stock = stock;
        return this;
    }

    public float getPrice() {
        return price;
    }

    public ProductResponse setPrice(float price) {
        this.price = price;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductResponse product = (ProductResponse) o;
        return stock == product.stock &&
                Float.compare(product.price, price) == 0 &&
                Objects.equals(id, product.id) &&
                Objects.equals(name, product.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, stock, price);
    }
}
